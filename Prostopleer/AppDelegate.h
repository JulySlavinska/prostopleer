//
//  AppDelegate.h
//  Prostopleer
//
//  Created by Jul S on 10/11/16.
//  Copyright © 2016 quadecco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

