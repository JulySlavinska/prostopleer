#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define BASE_URL @"http://api.pleer.com/"

@interface PPBaseAPIManager : NSObject
+ (instancetype)initManager;

- (void)postMethodWithURL:(NSURL *)webServiceUrl
               parameters:(id)requestParams
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure;
@end
