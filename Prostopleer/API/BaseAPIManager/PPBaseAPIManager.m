#import "PPBaseAPIManager.h"
#import <CommonCrypto/CommonDigest.h>

@implementation PPBaseAPIManager
+ (instancetype)initManager {
    return nil;
}

#pragma mark - HTTPManager

- (void)postMethodWithURL:(NSURL *)webServiceUrl
               parameters:(id)requestParams
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure {
    
    NSString *urlStr = [webServiceUrl absoluteString];
    
    
   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
 

    [manager POST:urlStr parameters:requestParams progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (success) {
            success(responseObject);
        }
        NSLog(@"Success: %@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //! for debug
        NSDictionary *errorData = [error userInfo];
        NSData *jsonData  = [errorData valueForKey:@"com.alamofire.serialization.response.error.data"];
        
        //! connection error
        if (jsonData == nil) {
            if (failure) {
                failure(error);
            }
            return;
        }
        
        NSError *parsingAPIerror = nil;
        __unused NSDictionary *errorDataDict  = nil;
        if (jsonData !=nil) {
            errorDataDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:NSJSONReadingMutableContainers
                                                              error:&parsingAPIerror];
        }
        
        //! string error if respons data not JSON
        NSString *str_error = @"";
        if (parsingAPIerror) {
            str_error = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            if (str_error) {
                failure([NSError errorWithDomain:@"test" code:12345 userInfo:@{NSLocalizedDescriptionKey :str_error}]);
                return;
            }
        }
        
        //! get current API error
        if (errorData) {
            id errorObj = [errorDataDict valueForKey:@"error"];
            
            if ([errorObj isKindOfClass:[NSString class]]) {
                if ([errorObj isEqualToString:@"invalid_token"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kInvalidToken object:nil userInfo:nil];
                    failure([NSError errorWithDomain:@"test" code:12345 userInfo:@{NSLocalizedDescriptionKey :[errorDataDict valueForKey:@"error_description"]}]);
                    return;
                }
            }
        }
        
        //! default error
        if (failure) {
            failure(error);
        }

    }];
}

@end
