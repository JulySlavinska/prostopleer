#define TRACK_RESOURCE @"resource.php"
#define TRACK_TOKEN @"token.php"
#define TRACK_INDEX @"index.php"
//credential
#define CLIENT_ID @"956065"
#define CLIENT_SECRET @"GNUJduYNcSDyHVwBeyGj"

#import "PPSongsManager.h"

@implementation PPSongsManager
+ (instancetype)initManager {
    return [[PPSongsManager alloc] init];
}

#pragma mark - POST_GET_TOKEN

- (void)getTokenWithSuccess:(void(^)(NSString *token))success
                   failure:(void(^)(NSError *error))failure {
    NSURL *token = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_TOKEN]];
    NSDictionary *parameters = @{@"grant_type": @"client_credentials",
                                 @"client_secret": CLIENT_SECRET,
                                 @"client_id": CLIENT_ID};
   [self postMethodWithURL:token parameters:parameters success:^(id responseObject) {
       if (success) {
           NSLog(@"%@", [responseObject valueForKey:@"access_token"]);
           success([responseObject valueForKey:@"access_token"]);
       }
   } failure:^(NSError *error) {
       if (failure) {
           failure(error);
       }
   }];
}

#pragma mark - POST_TRACKS_SEARCH

- (void)searchTrackWithString:(NSString *)searchString pageTrack:(NSInteger)page resultOnPage:(NSInteger)count qualityTrack:(NSString *)quality success:(void(^)(NSArray *tracksList, NSInteger total))success
                failure:(void(^)(NSError *error))failure {
    
    NSURL *webServiceUrl = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_INDEX]];
    NSDictionary *parameters = @{@"access_token": [PPCommon getToken],
                                 @"method": @"tracks_search",
                                 @"query": searchString,
                                 @"result_on_page": @(count),
                                 @"quality": quality,
                                 @"page": @(page)};
    
    [self postMethodWithURL:webServiceUrl parameters:parameters success:^(id responseObject) {
        if (success) {
            success([self tracksFromArray:[responseObject valueForKey:@"tracks"]],
                    [[responseObject valueForKey:@"count"] integerValue]);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
//////////////////////////////////////////////////
#pragma mark - POST_TRACKS_GET_INFO(совершенно бесполезный метод, но пусть живет)

- (void)getInfoTrackWithTrackID:(NSString *)trackID success:(void(^)(PPSongModel *model))success
                      failure:(void(^)(NSError *error))failure {
    
    NSURL *webServiceUrl = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_RESOURCE]];
    NSDictionary *parameters = @{@"access_token": [PPCommon getToken],
                                 @"track_id": trackID,
                                 @"method": @"tracks_get_info"};
    
    [self postMethodWithURL:webServiceUrl parameters:parameters success:^(id responseObject) {
        if (success) {
            success([PPSongModel fillFromDictionary:[responseObject objectForKey:@"data"]]);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
#pragma mark - POST_TRACKS_GET_LYRICS
- (void)lyricsTrackWithTrackID:(NSString *)trackID success:(void(^)(NSString *lyric))success
                      failure:(void(^)(NSError *error))failure {
    
    NSURL *webServiceUrl = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_RESOURCE]];
    NSDictionary *parameters = @{@"access_token": [PPCommon getToken],
                                 @"method": @"tracks_get_lyrics",
                                 @"track_id": trackID};
    
    [self postMethodWithURL:webServiceUrl parameters:parameters success:^(id responseObject) {
        
        if ([[responseObject valueForKey:@"text"] isKindOfClass:[NSNull class]]) {
            if (success) {
                success(@"");
                return;
            }
        }
        if (success) {
            success([responseObject valueForKey:@"text"]);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
#pragma mark - POST_TRSCKS_GET_DOWNLOAD_LINK
//reason @"listen" or @"save"
- (void)getDownloadLinkWithReason:(NSString *)reason trackID:(NSString *)trackID success:(void(^)(NSString *urlString))success
                      failure:(void(^)(NSError *error))failure {
    
    NSURL *webServiceUrl = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_RESOURCE]];
    NSDictionary *parameters = @{@"access_token": [PPCommon getToken],
                                 @"method": @"tracks_get_download_link",
                                 @"reason": reason,
                                 @"track_id": trackID};
    
    [self postMethodWithURL:webServiceUrl parameters:parameters success:^(id responseObject) {
        if (success) {
            success([responseObject valueForKey:@"url"]);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
#pragma mark - POST_GET_TOP_LIST

- (void)getTopListWithListType:(NSInteger)listType language:(NSString *)laguage  pageTrack:(NSInteger)page resultOnPage:(NSInteger)count success:(void(^)(NSArray *tracksList, NSInteger total))success failure:(void(^)(NSError *error))failure {
    
    NSURL *webServiceUrl = [NSURL URLWithString:[BASE_URL stringByAppendingString:TRACK_RESOURCE]];
    NSString *languageParam = laguage;
    if (!languageParam) {
        languageParam = @"en";
    }
    NSDictionary *parameters = @{@"access_token": [PPCommon getToken],
                                 @"method": @"get_top_list",
                                 @"list_type": @(listType),
                                 @"page":@(page),
                                 @"result_on_page": @(count),
                                 @"language":languageParam};
    
    [self postMethodWithURL:webServiceUrl parameters:parameters success:^(id responseObject) {
        if (success) {
            success([self tracksFromArray:[[responseObject valueForKey:@"tracks"] valueForKey:@"data"]],
                    [[[responseObject valueForKey:@"tracks"] valueForKey:@"count"] integerValue]);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
#pragma mark - POST_GET_SUGGEST
- (NSArray *)tracksFromArray:(NSDictionary *)tracks {
    NSMutableArray *array = [NSMutableArray new];
    for(NSString *trackId in tracks) {
        
        NSDictionary *trackData = tracks[trackId];
       [array addObject:[PPSongModel fillFromDictionary:trackData]];
    }
    return array;
}

@end
