#import <Foundation/Foundation.h>
#import "PPBaseAPIManager.h"
#define PARAMS_GET_TOKEN @"grant_type=client_credentials&client_secret=%@&client_id=%@"
#define PARAMS_SEARCH_TRACKS @"access_token=%@&method=tracks_search&query=%@"
@interface PPSongsManager : PPBaseAPIManager
+ (instancetype)initManager;

- (void)getTokenWithSuccess:(void(^)(NSString *token))success
                failure:(void(^)(NSError *error))failure;

- (void)searchTrackWithString:(NSString *)searchString pageTrack:(NSInteger)page resultOnPage:(NSInteger)count qualityTrack:(NSString *)quality success:(void(^)(NSArray *tracksList, NSInteger total))success
                      failure:(void(^)(NSError *error))failure;

- (void)getInfoTrackWithTrackID:(NSString *)trackID success:(void(^)(PPSongModel *model))success
                     failure:(void(^)(NSError *error))failure;

- (void)lyricsTrackWithTrackID:(NSString *)trackID success:(void(^)(NSString *lyric))success
                       failure:(void(^)(NSError *error))failure;

- (void)getDownloadLinkWithReason:(NSString *)reason trackID:(NSString *)trackID success:(void(^)(NSString *urlString))success
                failure:(void(^)(NSError *error))failure;

- (void)getTopListWithListType:(NSInteger)listType language:(NSString *)laguage  pageTrack:(NSInteger)page resultOnPage:(NSInteger)count success:(void(^)(NSArray *tracksList, NSInteger total))success failure:(void(^)(NSError *error))failure;
@end
