#import "PPSongModel.h"

@implementation PPSongModel
+ (instancetype)fillFromDictionary:(NSDictionary *)dictionary{
    PPSongModel *model = [PPSongModel new];
    model.trackId = [dictionary safeStringForKey:@"id"];
    model.artist = [dictionary safeStringForKey:@"artist"];
    model.track = [dictionary safeStringForKey:@"track"];
    model.length = [dictionary safeNumberForKey:@"length"];
    model.bitrate = [dictionary safeStringForKey:@"bitrate"];
    model.server_id4 = [dictionary safeNumberForKey:@"server_id4"];
    model.textID = [dictionary safeNumberForKey:@"text_id"];
    return model;
}

@end
