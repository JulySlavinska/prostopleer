#import <Foundation/Foundation.h>

@interface PPSongModel : NSObject
@property (nonatomic, strong) NSString *trackId;
@property (nonatomic, strong) NSString *artist;
@property (nonatomic, strong) NSString *track;
@property (strong, nonatomic) NSNumber *length;
@property (nonatomic, strong) NSString *bitrate;
@property (strong, nonatomic) NSNumber *server_id4;
@property (nonatomic, strong) NSNumber *textID;

+ (instancetype)fillFromDictionary:(NSDictionary *)dictionaty;
@end
