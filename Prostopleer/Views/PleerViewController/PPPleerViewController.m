#import "PPPleerViewController.h"

@interface PPPleerViewController ()
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameTrack;
@property (weak, nonatomic) IBOutlet UILabel *artistTrack;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *stopBtn;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

@implementation PPPleerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
#pragma mark - Setup UI

- (void) setSongModel:(PPSongModel *)songModel{
    _songModel = songModel;
    self.nameTrack.text = self.songModel.track;
    self.artistTrack.text = self.songModel.artist;
}
#pragma mark - Actions
- (IBAction)closeBtnAction:(id)sender {
    if (self.closePleer) {
        self.closePleer();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backBtnAction:(id)sender {
    if (self.backTrack) {
        self.backTrack();
    }
}

- (IBAction)stopBtnAction:(id)sender {
    if (self.stopTrack) {
        self.stopTrack();
    }
}

- (IBAction)playBtnAction:(id)sender {
    if (self.playTrack) {
        self.playTrack(self.progressTrack);
    }
}

- (IBAction)nextBtnAction:(id)sender {
    if (self.nextTrack) {
        self.nextTrack(self);
    }
}

- (IBAction)sliderProgress:(UISlider *)sender {
    if (self.sligerProgressTrack) {
        self.sligerProgressTrack(sender);
    }
}


@end
