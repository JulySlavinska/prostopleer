#import <UIKit/UIKit.h>

@interface PPPleerViewController : PPBaseViewController
@property (copy, nonatomic) void (^closePleer)();
@property (copy, nonatomic) void (^backTrack)();
@property (copy, nonatomic) void (^stopTrack)();
@property (copy, nonatomic) void (^playTrack)(UISlider *sender);
@property (copy, nonatomic) void (^nextTrack)(PPPleerViewController *controll);
@property (copy, nonatomic) void (^sligerProgressTrack)(UISlider *sender);
@property (strong, nonatomic) PPSongModel *songModel;

@property (weak, nonatomic) IBOutlet UISlider *progressTrack;
@end
