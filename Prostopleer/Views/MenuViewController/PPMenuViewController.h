#import <UIKit/UIKit.h>

@interface PPMenuViewController : PPBaseViewController
@property (copy, nonatomic) void (^closeMenu)(NSUInteger indexRow, NSString *nameMenu);

- (void) setDataSource:(NSArray *)dataSource;
@property (assign, nonatomic) NSUInteger selectedMenu;
@property (strong, nonatomic) NSString *selectedMenuTitle;
@end
