#import "PPMenuViewController.h"
#import "PPMenuCell.h"

static NSString *const kMenuCell = @"menuCell";
#define HEIGHT_MENU_CELL 47
#define TOP_LAYOUT_CONSTRAIN 45

@interface PPMenuViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayoutConstaint;
@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation PPMenuViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.userInteractionEnabled = YES;
    self.view.multipleTouchEnabled = YES;
    self.tableView.layer.cornerRadius = 5;
    self.tableView.layer.borderWidth = 2;
    self.tableView.layer.borderColor = [UIColor orangeColor].CGColor;
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - setters

- (void) setDataSource:(NSArray *)dataSource{
    _dataSource = dataSource;
    self.bottomLayoutConstaint.constant = self.view.frame.size.height-21-TOP_LAYOUT_CONSTRAIN-HEIGHT_MENU_CELL*self.dataSource.count;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    PPMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:kMenuCell];
    
    NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
    cell.titleLbl.text = [dict objectForKey:@"title"];
    
    if (indexPath.row == self.selectedMenu&&self.selectedMenuTitle.length) {
        [cell setBackgroundColor:[UIColor orangeColor]];
    }else{
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = [self.dataSource objectAtIndex:indexPath.row];
    
    if (self.closeMenu) {
        self.closeMenu(indexPath.row, [dict objectForKey:@"title"]);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Toches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint loc = [touch locationInView:self.view];
    
    CGRect frame = self.tableView.frame;
    frame.size.height = self.tableView.contentSize.height;
    
    if (!CGRectContainsPoint(frame, loc)) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
