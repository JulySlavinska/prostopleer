#import "PPCommon.h"
#define kTokenKey @"access_token"

@implementation PPCommon

#pragma mark - Token

+ (void)saveToken:(NSString *)token{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kTokenKey];
}

+ (NSString *)getToken{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
}
@end
