#ifndef PPMacros_h
#define PPMacros_h

#ifdef DEBUG
#define DLog(format, ...) NSLog((@"%d %s " format), __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif
#endif
