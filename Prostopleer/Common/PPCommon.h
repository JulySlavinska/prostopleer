#import <Foundation/Foundation.h>

@interface PPCommon : NSObject
+ (void)saveToken:(NSString *)token;
+ (NSString *)getToken;
@end
