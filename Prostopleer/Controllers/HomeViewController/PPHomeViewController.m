#import "PPHomeViewController.h"
#import "PPSearchViewController.h"

@interface PPHomeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *helloTextLbl;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end

@implementation PPHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)loginBtnAction:(id)sender {
    __weak PPHomeViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[PPSongsManager initManager] getTokenWithSuccess:^(NSString *token) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [PPCommon saveToken:token];
        if ([PPCommon getToken].length) {
            PPSearchViewController *searchVC = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"PPSearchViewController"];
            [weakSelf.navigationController pushViewController:searchVC animated:YES];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
             
    }];
}


@end
