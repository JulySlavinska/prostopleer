#import <UIKit/UIKit.h>

@interface PPBaseViewController : UIViewController
@property (assign, nonatomic, readonly) CGFloat keyBoardHeight;
@property (strong, nonatomic) UIView *editView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
//keyboard
- (void)willShowKeyBoardWithSize:(CGSize)size;
- (void)didShowKeyBoardWithSize:(CGSize)size;
- (void)didHideKeyBoardWithSize:(CGSize)size;

- (void)showAllerWIthTitle:(NSString *)title
                   message:(NSString *)message
               acceptBlock:(void(^)())accept cancel:(void(^)())cencel;

- (void)showAllerWIthTitle:(NSString *)title
                   message:(NSString *)message
               acceptTitle:(NSString *)atitle
               acceptBlock:(void(^)())accept cancel:(void(^)())cencel;

- (void)showErrorAlertWithTitle:(NSString *)title
                        message:(NSString *)message
                     completion:(void(^)())completion;
- (void)showAllerWIthTitle:(NSString *)title
                   message:(NSString *)message
               acceptBlock:(void(^)())accept;

- (void)commonPresentViewController:(UIViewController*)viewController animated:(BOOL)animated;

@end
