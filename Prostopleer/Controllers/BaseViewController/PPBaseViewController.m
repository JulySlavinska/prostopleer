#import "PPBaseViewController.h"
#import "PPHomeViewController.h"

@interface PPBaseViewController ()
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (assign, nonatomic) CGFloat keyBoardHeight;
@end

@implementation PPBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupBlackNavigationBar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMainScreen) name:kInvalidToken object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInvalidToken object:nil];
}

#pragma mark - AlertController

- (void)showAllerWIthTitle:(NSString *)title message:(NSString *)message acceptBlock:(void(^)())accept cancel:(void(^)())cencel {
    [self showAllerWIthTitle:title message:message acceptTitle:@"OK" acceptBlock:accept cancel:cencel];
}

- (void)showAllerWIthTitle:(NSString *)title message:(NSString *)message acceptBlock:(void(^)())accept{
    UIAlertController *actioSheet = [UIAlertController alertControllerWithTitle:title
                                                                        message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *accAction = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                            if (accept) {
                                                                accept();
                                                            }
                                                        }];
    
    [actioSheet addAction:accAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actioSheet animated:YES completion:nil];
    });
}

- (void)showAllerWIthTitle:(NSString *)title
                   message:(NSString *)message
               acceptTitle:(NSString *)atitle
               acceptBlock:(void(^)())accept cancel:(void(^)())cencel {
    UIAlertController *actioSheet = [UIAlertController alertControllerWithTitle:title
                                                                        message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"BranchInfo.PhoneAlert.Cancel", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (cencel) {
            cencel();
        }
    }];
    [actioSheet addAction:cancelAction];
    
    UIAlertAction *accAction = [UIAlertAction actionWithTitle:atitle
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                            if (accept) {
                                                                accept();
                                                            }
                                                        }];
    
    [actioSheet addAction:accAction];
    
     dispatch_async(dispatch_get_main_queue(), ^{
         [self presentViewController:actioSheet animated:YES completion:nil];
    });
}

- (void)showErrorAlertWithTitle:(NSString *)title
                        message:(NSString *)message
                     completion:(void(^)())completion {
   
    UIAlertController *actioSheet = [UIAlertController alertControllerWithTitle:title
                                                                        message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (completion) {
            completion();
        }
    }];
    [actioSheet addAction:okAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actioSheet animated:YES completion:nil];
    });
}



#pragma mark - UIKeyboardNotification

- (void)keyboardWillShow:(NSNotification *)notif {
    NSDictionary* info = [notif userInfo];
    
    if (self.tapGesture == nil) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidekeyBoard)];
    }
    
    [self.view addGestureRecognizer:self.tapGesture];
    
    self.keyBoardHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self willShowKeyBoardWithSize:[[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size];
    
}

- (void)keyboardDidShow:(NSNotification *)notif {
    NSDictionary* info = [notif userInfo];
    
    if (self.tapGesture == nil) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidekeyBoard)];
    }
    
    [self.view addGestureRecognizer:self.tapGesture];
    
    self.keyBoardHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self didShowKeyBoardWithSize:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size];
}

- (void)keyboardDidHide:(NSNotification *)notif {
    NSDictionary* info = [notif userInfo];
    [self.view removeGestureRecognizer:self.tapGesture];
    [self didHideKeyBoardWithSize:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size];
    self.keyBoardHeight = 0;
}

- (void)hidekeyBoard {
    
    [self.view endEditing:YES];
}

#pragma mark - Helper

- (void)willShowKeyBoardWithSize:(CGSize)size {
    
}

- (void)didShowKeyBoardWithSize:(CGSize)size {
    
}

- (void)didHideKeyBoardWithSize:(CGSize)size {
    
}

#pragma mark - Gesture Recognizer

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (void)showMainScreen{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PPHomeViewController *homeVC = [storyboard instantiateViewControllerWithIdentifier:@"PPHomeViewController"];
    UINavigationController *homeNavigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    [[[UIApplication sharedApplication] delegate] window].rootViewController = homeNavigationController;
}

#pragma mark - Navigation Bar

- (void)setupBlackNavigationBar{
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    navigationBar.hidden = NO;
    
    
    [navigationBar setBackgroundImage:[UIImage new]
                           forBarPosition:UIBarPositionAny
                               barMetrics:UIBarMetricsDefault];
        
    [navigationBar setShadowImage:[UIImage new]];
    
    [navigationBar setBarTintColor:[UIColor blackColor]];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setBackgroundColor:[UIColor blackColor]];

    navigationBar.barStyle = UIBarStyleBlack;

    [navigationBar setTranslucent:NO];
    
}

#pragma mark - Common Present View Controller

- (void)commonPresentViewController:(UIViewController*)viewController animated:(BOOL)animated {
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:viewController animated:animated completion:nil];
}
@end
