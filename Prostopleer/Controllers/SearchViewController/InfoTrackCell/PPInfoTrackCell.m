#import "PPInfoTrackCell.h"

@interface PPInfoTrackCell ()

@property (weak, nonatomic) IBOutlet UILabel *trackInfoLbl;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UILabel *lyricsLbl;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoBtn;

@property (strong, nonatomic) PPSongModel *track;
@end

@implementation PPInfoTrackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}
#pragma mark - setters
- (void) setSongs:(PPSongModel *)track withLyric:(NSString *)lyric withTrackID :(NSString *)trackID {
    self.track = track;
    self.trackInfoLbl.text = [NSString stringWithFormat:@"%@ - %@", track.artist, track.track];
    if ([track.trackId isEqualToString:trackID]&&lyric.length) {
        self.lyricsLbl.text = lyric;
    }else{
        self.lyricsLbl.text = nil;
    }
}
- (void)setIsPlaing:(BOOL)isPlaing{
    _isPlaing = isPlaing;
    if (_isPlaing) {
        self.trackInfoLbl.textColor = [UIColor redColor];
        self.playBtn.hidden = YES;
    }else{
        self.playBtn.hidden = NO;
        self.trackInfoLbl.textColor = [UIColor orangeColor];
        [self.playBtn setTitle:@"play" forState:UIControlStateNormal];
    }
}
#pragma mark - Action
- (IBAction)playBtnAction:(id)sender {
    if (self.playTrack) {
        self.playTrack(self, self.track.trackId, self.indexRow);
    }
}
- (IBAction)moreInfoBtnAction:(id)sender {
    if (self.openMoreInfo) {
        self.openMoreInfo(self, self.track.trackId);
    }
}

@end
