#import <UIKit/UIKit.h>

@interface PPInfoTrackCell : UITableViewCell

//@property (assign, nonatomic) NSUInteger indexRow;
@property (copy, nonatomic) void (^openMoreInfo)(PPInfoTrackCell *cell,  NSString *trackID);
@property (copy, nonatomic) void (^playTrack)(PPInfoTrackCell *cell, NSString *trackID, NSUInteger indexRow);

- (void)setSongs:(PPSongModel *)track withLyric:(NSString *)lyric withTrackID :(NSString *)trackID;
@property (assign, nonatomic) BOOL isPlaing;
@property (assign, nonatomic) NSUInteger indexRow;
@end
