#import "PPSearchViewController.h"
#import "PPInfoTrackCell.h"
#import "PPMoreInfoVoewController.h"
#import "PPMenuViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PPPleerViewController.h"

static NSString *const kInfoCell = @"infoCell";

#define COUNT_LOAD_TRACK_ON_PAGE 20

@interface PPSearchViewController ()<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) NSInteger totalCount;
@property (strong, nonatomic) NSString *qualityTrack;
@property (assign, nonatomic) NSInteger nextPage;

@property (strong, nonatomic) NSString *trackID;
@property (strong, nonatomic) NSString *nextTrackID;
@property (strong, nonatomic) NSString *lyricTrack;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property (assign, nonatomic) NSInteger topIndex;
@property (strong, nonatomic) NSString *topTitle;
@property (assign, nonatomic) NSInteger langIndex;
@property (strong, nonatomic) NSString *langTitle;

@property (assign, nonatomic) NSInteger selectedIndex;
//pleer on bottom
@property (weak, nonatomic) IBOutlet UILabel *trackNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *artistTrackLbl;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPleerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSearchConstr;
@property (weak, nonatomic) IBOutlet UIView *bottomPleerView;

@property (weak, nonatomic) IBOutlet UIButton *showPleerBtn;

@property (strong, nonatomic) PPSongModel *songModelPlaying;
@property (strong, nonatomic) NSTimer *sliderTimer;
@property (strong, nonatomic) UISlider *slider;

@end

@implementation PPSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getDataSourceTracks];
    [self setupRefresh];
    self.heightPleerView.constant = 0;
    self.bottomPleerView.hidden = YES;
    
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.tableView setShowsPullToRefresh:YES];
    [self setupNavigation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.tableView.editing = NO;
    
    [self.tableView.pullToRefreshView stopAnimating];
    [self.tableView setShowsPullToRefresh:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UI

- (void) setupUI{
    self.searchBar.hidden = NO;
    self.heightSearchConstr.constant = 44;
    self.topTitle = nil;
    self.langTitle = nil;
}

#pragma mark - Navigation

- (void)setupNavigation{
    UIButton *sortTopButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [sortTopButton setTitle:@"top" forState:UIControlStateNormal];
    [sortTopButton addTarget:self action:@selector(showSortMenuTop) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *sortTopButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortTopButton];
    
    UIButton *sortLangButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [sortLangButton setTitle:@"lang" forState:UIControlStateNormal];
    [sortLangButton addTarget:self action:@selector(showSortLanguageMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *sortLangButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortLangButton];
    
    self.navigationItem.rightBarButtonItems = @[sortTopButtonItem, sortLangButtonItem];
   
}

- (void)showSortMenuTop{
    __weak PPSearchViewController *weakSelf = self;
    PPMenuViewController *menuPopup  = [self.storyboard instantiateViewControllerWithIdentifier:@"PPMenuViewController"];
    menuPopup.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    menuPopup.view.backgroundColor = [UIColor clearColor];
    
    menuPopup.selectedMenu = self.topIndex;
    menuPopup.selectedMenuTitle = self.topTitle;
    
    [menuPopup setDataSource:[self topDataSource]];
    [self commonPresentViewController:menuPopup animated:YES];
    [menuPopup setCloseMenu:^(NSUInteger indexRow, NSString *nameMenu) {
        weakSelf.topTitle = nameMenu;
        weakSelf.topIndex = indexRow;
        [weakSelf getTopListTrack];
    }];
}

- (void)showSortLanguageMenu{
    __weak PPSearchViewController *weakSelf = self;
    PPMenuViewController *menuPopup  = [self.storyboard instantiateViewControllerWithIdentifier:@"PPMenuViewController"];
    menuPopup.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    menuPopup.view.backgroundColor = [UIColor clearColor];
    
    menuPopup.selectedMenu = self.langIndex;
    menuPopup.selectedMenuTitle = self.langTitle;
    
    [menuPopup setDataSource:[self languageDataSource]];
    
    [self commonPresentViewController:menuPopup animated:YES];
   [menuPopup setCloseMenu:^(NSUInteger indexRow, NSString *nameMenu) {
        weakSelf.langTitle = nameMenu;
        weakSelf.langIndex = indexRow;
        [weakSelf getTopListTrack];
        
    }];
}
- (NSInteger )returnPeriodWithIndex:(NSInteger)indexRow{
    NSDictionary *dictObj = [[self topDataSource] objectAtIndex:indexRow];
    return [[dictObj objectForKey:@"index"] integerValue];
}
- (NSString *)returnTypeLanguadeWithIndex:(NSInteger)indexRow{
    NSDictionary *dictObj = [[self languageDataSource] objectAtIndex:indexRow];
    return [dictObj objectForKey:@"ident"];
}
#pragma mark - setup datasource for menu

- (NSArray *)topDataSource{
    NSMutableArray *topMutArr = [NSMutableArray new];
    
    [topMutArr addObject:@{@"title":@"week",
                           @"index":@(1)}];
    [topMutArr addObject:@{@"title":@"month",
                           @"index":@(2)}];
    [topMutArr addObject:@{@"title":@"3 months",
                           @"index":@(3)}];
    [topMutArr addObject:@{@"title":@"half-year",
                           @"index":@(4)}];
    [topMutArr addObject:@{@"title":@"year",
                           @"index":@(5)}];
    return topMutArr;
}

- (NSArray *)languageDataSource{
    NSMutableArray *langMutArr = [NSMutableArray new];
    
    [langMutArr addObject:@{@"title":@"en",
                           @"ident":@"en"}];
    [langMutArr addObject:@{@"title":@"ru",
                           @"ident":@"ru"}];
    
    return langMutArr;
}

#pragma mark - Helper

- (void)setupRefresh {
    
   __weak PPSearchViewController *weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf getDataSourceTracks];
        [weakSelf setupUI];
        
    }];
    self.tableView.pullToRefreshView.textColor = [UIColor lightGrayColor];
    self.tableView.pullToRefreshView.arrowColor = [UIColor lightGrayColor];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
         [weakSelf loadNextDataSource];
    }];
}

#pragma mark - Setup DataSource

- (void)getDataSourceTracks{
    self.qualityTrack = @"all";
    self.nextPage = 1;
    __weak PPSearchViewController *weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
    [[PPSongsManager initManager]searchTrackWithString:self.searchBar.text pageTrack:self.nextPage resultOnPage:COUNT_LOAD_TRACK_ON_PAGE qualityTrack:self.qualityTrack success:^(NSArray *tracksList, NSInteger total) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf.tableView.pullToRefreshView stopAnimating];
        weakSelf.dataSource = tracksList;
        weakSelf.totalCount = total;
        weakSelf.nextPage += 1;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf.tableView.pullToRefreshView stopAnimating];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];
    
}
- (void)loadNextDataSource{
    __weak PPSearchViewController *weakSelf = self;
    
    
    [[PPSongsManager initManager]searchTrackWithString:self.searchBar.text pageTrack:self.nextPage resultOnPage:COUNT_LOAD_TRACK_ON_PAGE qualityTrack:self.qualityTrack success:^(NSArray *tracksList, NSInteger total) {
         [weakSelf.tableView.infiniteScrollingView stopAnimating];
        
        if (weakSelf.dataSource.count < total) {
            NSMutableArray *tracksMutable = [NSMutableArray new];
            tracksMutable = [weakSelf.dataSource mutableCopy];
            [tracksMutable addObjectsFromArray:tracksList];
            weakSelf.dataSource = tracksMutable;
        }
        weakSelf.totalCount = total;
        weakSelf.nextPage += 1;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];
}
//основная инфа
- (void)getTrackInfoWithTrackID:(NSString *)trackId {
    __weak PPSearchViewController *weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[PPSongsManager initManager]getInfoTrackWithTrackID:trackId success:^(PPSongModel *model) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        
        [weakSelf openMoreInfoTrack:model];
        NSLog(@"%@",model);
    } failure:^(NSError *error) {
        
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];
}
//слова
- (void)getLyricWithTrackIDFoeIndexRow:(NSInteger )indexRow{
    __weak PPSearchViewController *weakSelf = self;
    PPSongModel *model = [self.dataSource objectAtIndex:indexRow];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[PPSongsManager initManager]lyricsTrackWithTrackID:model.trackId success:^(NSString *lyric) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        
        weakSelf.lyricTrack = lyric;
        weakSelf.trackID = model.trackId;
        weakSelf.selectedIndex = indexRow;
        [weakSelf.tableView reloadData];
        
        DLog(@"Слова /n %@", lyric);
    } failure:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];
}
//получение ссылки на прослушивание файла
- (void)getLinkWithTrackId:(NSString *)trackId success:(void(^)()) success {
    __weak PPSearchViewController *weakSelf = self;
   
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
     [[PPSongsManager initManager] getDownloadLinkWithReason:@"listen" trackID:trackId success:^(NSString *urlString) {
         [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
         
         if (urlString) {
             
             NSURL *myurl = [NSURL URLWithString:urlString];
             NSData *data = [NSData dataWithContentsOfURL:myurl];
             weakSelf.audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:nil];
             weakSelf.audioPlayer.delegate = weakSelf;
             [weakSelf.audioPlayer play];
             weakSelf.nextTrackID = trackId;
            
            weakSelf.songModelPlaying = [weakSelf getTrackModelWithTrackID:trackId];
             if (weakSelf.songModelPlaying!=nil) {
                 weakSelf.trackNameLbl.text = weakSelf.songModelPlaying.track;
                 weakSelf.artistTrackLbl.text = weakSelf.songModelPlaying.artist;
                 [weakSelf.playPauseBtn setTitle:@"pause" forState:UIControlStateNormal];
                 weakSelf.heightPleerView.constant = 71;
                 weakSelf.bottomPleerView.hidden = NO;
                 [weakSelf.tableView reloadData];
                 if (success) {
                     success();
                 }
             }
         }
         DLog(@"%@", urlString);
     } failure:^(NSError *error) {
         [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
         [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
     }];
}
//получить топ есть 2 вида получения по языку и по периоду
-(void) getTopListTrack{
    __weak PPSearchViewController *weakSelf = self;
    self.nextPage = 1;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [[PPSongsManager initManager]getTopListWithListType:[self returnPeriodWithIndex:self.topIndex] language:[self returnTypeLanguadeWithIndex:self.langIndex] pageTrack:self.nextPage resultOnPage:COUNT_LOAD_TRACK_ON_PAGE success:^(NSArray *tracksList, NSInteger total) {
        
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf.tableView.pullToRefreshView stopAnimating];
        
        weakSelf.dataSource = tracksList;
        weakSelf.totalCount = total;
        weakSelf.nextPage += 1;
        [weakSelf.tableView reloadData];
        weakSelf.searchBar.hidden = YES;
        weakSelf.heightSearchConstr.constant = 0;
        [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{
            [weakSelf loadNextTopList];
        }];
    } failure:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
        [weakSelf.tableView.pullToRefreshView stopAnimating];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];

}

- (void)loadNextTopList{
    __weak PPSearchViewController *weakSelf = self;
    [[PPSongsManager initManager]getTopListWithListType:[self returnPeriodWithIndex:self.topIndex] language:[self returnTypeLanguadeWithIndex:self.langIndex] pageTrack:self.nextPage resultOnPage:COUNT_LOAD_TRACK_ON_PAGE success:^(NSArray *tracksList, NSInteger total) {
        
        if (weakSelf.dataSource.count < total) {
            NSMutableArray *tracksMutable = [NSMutableArray new];
            tracksMutable = [weakSelf.dataSource mutableCopy];
            [tracksMutable addObjectsFromArray:tracksList];
            weakSelf.dataSource = tracksMutable;
        }
        weakSelf.totalCount = total;
        weakSelf.nextPage += 1;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
        [weakSelf showErrorAlertWithTitle:nil message:error.localizedDescription completion:nil];
    }];
}

- (void)playNextSong:(void(^)()) success {
    NSString *idToPlay = @"";
    for (PPSongModel *song in self.dataSource) {
        if ([song.trackId isEqualToString:self.nextTrackID]) {
            NSInteger indexRow = [self.dataSource indexOfObject:song];
            if (indexRow+1<=self.dataSource.count-1) {
                
                PPSongModel *songModel = [self.dataSource objectAtIndex:indexRow+1];
                idToPlay = songModel.trackId;
                [self getLinkWithTrackId:idToPlay success:^{
                    if (success) {
                        success();
                    }
                }];
            }
        }
    }
}

- (PPSongModel *)getTrackModelWithTrackID:(NSString *)trackID{
    for (PPSongModel *songModel in self.dataSource) {
        if ([trackID isEqualToString:songModel.trackId]) {
            return songModel;
        }
    }
    return nil;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 500;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak PPSearchViewController *weakSelf = self;
    PPInfoTrackCell *cell = [tableView dequeueReusableCellWithIdentifier:kInfoCell];
    
    PPSongModel *track = [self.dataSource objectAtIndex:indexPath.row];
    
    if ([self.nextTrackID isEqualToString:track.trackId]) {
        [cell setIsPlaing:YES];
        
    }else{
        [cell setIsPlaing:NO];
    }
    
    [cell setSongs:track withLyric:self.lyricTrack withTrackID:self.trackID];
   
    [cell setOpenMoreInfo:^(PPInfoTrackCell *cell, NSString *trackID) {
         [weakSelf getTrackInfoWithTrackID:trackID];
    }];
    
    [cell setPlayTrack:^(PPInfoTrackCell *cell, NSString *trackID, NSUInteger indexRow) {
        [cell setIsPlaing:YES];
        weakSelf.nextTrackID = trackID;
        [weakSelf getLinkWithTrackId:trackID success:nil];
    }];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if ( self.lyricTrack.length&&self.selectedIndex == indexPath.row) {
        self.lyricTrack = nil;
        [self.tableView reloadData];
    }else{
        [self getLyricWithTrackIDFoeIndexRow:indexPath.row];
    }
}

#pragma mark - Table View Actions

- (void)openMoreInfoTrack: (PPSongModel *)songModel{
    PPMoreInfoVoewController *moreVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PPMoreInfoVoewController"];
    moreVC.songModel = songModel;
    [self.navigationController pushViewController:moreVC animated:YES];
}

#pragma mark - Search bar

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [self getDataSourceTracks];
}

#pragma  mark - Audio pleer deledate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self.audioPlayer stop];
    [self playNextSong:nil];
    NSLog(@"Finished Playing");
    if (flag) {
        [self.sliderTimer invalidate];
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    NSLog(@"Something wrong");
}

#pragma mark - Actions

- (IBAction)playPauseBtnAction:(id)sender {
    
    if (self.audioPlayer.isPlaying) {
        [self.audioPlayer pause];
        [self.playPauseBtn setTitle:@"play" forState:UIControlStateNormal];
        
    }else{
        [self.audioPlayer play];
        [self.playPauseBtn setTitle:@"pause" forState:UIControlStateNormal];
    }
}

- (IBAction)nextBtnAction:(id)sender {
    [self.audioPlayer stop];
    [self playNextSong:nil];
}
- (IBAction)showPleerFullScreen:(id)sender {
    
    __weak PPSearchViewController *weakSelf = self;
    PPPleerViewController *pleerPopup  = [self.storyboard instantiateViewControllerWithIdentifier:@"PPPleerViewController"];
    pleerPopup.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    pleerPopup.view.backgroundColor = [UIColor clearColor];
    [pleerPopup setSongModel:self.songModelPlaying];
    pleerPopup.progressTrack.maximumValue = self.audioPlayer.duration;
    pleerPopup.progressTrack.value = self.audioPlayer.currentTime;
    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
    self.slider = pleerPopup.progressTrack;
    
    [self commonPresentViewController:pleerPopup animated:YES];
    
    [pleerPopup setBackTrack:^{
        
    }];
    
    [pleerPopup setStopTrack:^{
        [weakSelf.audioPlayer stop];
    }];
    
    [pleerPopup setPlayTrack:^(UISlider *aSlider){
        weakSelf.slider = aSlider;
        weakSelf.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        weakSelf.slider.maximumValue = weakSelf.audioPlayer.duration;
    
        [weakSelf.audioPlayer prepareToPlay];
        [weakSelf.audioPlayer play];
    }];
    
    [pleerPopup setNextTrack:^(PPPleerViewController *controll){
        [weakSelf.audioPlayer stop];
        [weakSelf playNextSong:^{
            controll.songModel = weakSelf.songModelPlaying;
        }];
        
    }];
    [pleerPopup setSligerProgressTrack:^(UISlider *aSlider) {
        [weakSelf.audioPlayer stop];
        [weakSelf.audioPlayer setCurrentTime:aSlider.value];
        [weakSelf.audioPlayer prepareToPlay];
        [weakSelf.audioPlayer play];
    }];

}


- (void)updateSlider {
    
    _slider.value = self.audioPlayer.currentTime;
}

#pragma mark - Keyboard Show/Hide

- (void)willShowKeyBoardWithSize:(CGSize)size {
    
    [self.view layoutIfNeeded];
    if (self.keyBoardHeight > 0) {
        self.bottomConstraint.constant = size.height;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view layoutIfNeeded];
        } completion:nil];
        return;
    }
    
    self.bottomConstraint.constant = size.height;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)didHideKeyBoardWithSize:(CGSize)size {
    
    [self.view layoutIfNeeded];
    
    self.bottomConstraint.constant -= size.height;
}
@end
