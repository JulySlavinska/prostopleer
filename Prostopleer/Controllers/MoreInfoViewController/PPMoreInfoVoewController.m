#import "PPMoreInfoVoewController.h"

@interface PPMoreInfoVoewController ()
@property (weak, nonatomic) IBOutlet UILabel *moreInfoLbl;

@end

@implementation PPMoreInfoVoewController

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"%@", self.songModel);
    self.moreInfoLbl.text = self.songModel.track;
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
