#import "NSDictionary+FBSDictionary.h"

@implementation NSDictionary (FBSDictionary)
- (NSDictionary *)dictionaryByRemovingKey:(NSString *)key{
    NSMutableDictionary *tmpDict = [self mutableCopy];
    [tmpDict removeObjectForKey:key];
    return [tmpDict copy];
}

- (NSString *)safeStringForKey:(NSString *)key{
    return [self checkObjectForKey:key class:[NSString class] returnValue:@""];
}

- (NSString *)safeIDStringForKey:(NSString *)key{
    if([self objectForKey:key] && [[self objectForKey:key] isKindOfClass:[NSNumber class]]){
        return [[self checkObjectForKey:key class:[NSNumber class] returnValue:[NSNumber numberWithInt:0]] stringValue];
    }else{
        return [self checkObjectForKey:key class:[NSString class] returnValue:@""];
    }
}

- (NSNumber *)safeNumberForKey:(NSString *)key{
    return [self checkObjectForKey:key class:[NSNumber class] returnValue:[NSNumber numberWithInt:0]];
}

- (BOOL)safeBoolForKey:(NSString *)key{
    return [[self checkObjectForKey:key class:[NSNumber class] returnValue:[NSNumber numberWithInt:0]] boolValue];
}

- (NSArray *)safeArrayForKey:(NSString *)key{
    return [self checkObjectForKey:key class:[NSArray class] returnValue:[NSArray array]];
}

- (id)checkObjectForKey:(NSString *)key class:(__unsafe_unretained Class)class returnValue:(id)value{
    id object = [self objectForKey:key];
    if (object == nil ) {
        return value;
    }
    if ([object isKindOfClass:class]) {
        return object;
    }
    return value;
}

@end
