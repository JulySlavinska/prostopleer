@interface NSDictionary (FBSDictionary)

- (NSDictionary *)dictionaryByRemovingKey:(NSString *)key;
- (NSString *)safeStringForKey:(NSString *)key;
- (NSString *)safeIDStringForKey:(NSString *)key;

- (NSNumber *)safeNumberForKey:(NSString *)key;
- (NSArray *)safeArrayForKey:(NSString *)key;

- (BOOL)safeBoolForKey:(NSString *)key;

@end
