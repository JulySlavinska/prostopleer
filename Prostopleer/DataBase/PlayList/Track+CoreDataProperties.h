//
//  Track+CoreDataProperties.h
//  Prostopleer
//
//  Created by Jul S on 10/24/16.
//  Copyright © 2016 quadecco. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Track.h"

NS_ASSUME_NONNULL_BEGIN

@interface Track (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *textID;
@property (nullable, nonatomic, retain) NSString *server_id4;
@property (nullable, nonatomic, retain) NSString *bitrate;
@property (nullable, nonatomic, retain) NSString *length;
@property (nullable, nonatomic, retain) NSString *track;
@property (nullable, nonatomic, retain) NSString *artist;
@property (nullable, nonatomic, retain) NSString *trackId;
@property (nullable, nonatomic, retain) NSString *link;
@property (nullable, nonatomic, retain) Playlist *relationship;

@end

NS_ASSUME_NONNULL_END
