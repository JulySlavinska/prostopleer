//
//  Playlist.h
//  Prostopleer
//
//  Created by Jul S on 10/24/16.
//  Copyright © 2016 quadecco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Track;

NS_ASSUME_NONNULL_BEGIN

@interface Playlist : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Playlist+CoreDataProperties.h"
