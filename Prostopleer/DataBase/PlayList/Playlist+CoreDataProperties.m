#import "Playlist+CoreDataProperties.h"

@implementation Playlist (CoreDataProperties)

@dynamic playlistID;
@dynamic name;
@dynamic date;
@dynamic count;
@dynamic relationship;

@end
