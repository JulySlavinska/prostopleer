//
//  Track+CoreDataProperties.m
//  Prostopleer
//
//  Created by Jul S on 10/24/16.
//  Copyright © 2016 quadecco. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Track+CoreDataProperties.h"

@implementation Track (CoreDataProperties)

@dynamic textID;
@dynamic server_id4;
@dynamic bitrate;
@dynamic length;
@dynamic track;
@dynamic artist;
@dynamic trackId;
@dynamic link;
@dynamic relationship;

@end
