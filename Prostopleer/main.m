//
//  main.m
//  Prostopleer
//
//  Created by Jul S on 10/11/16.
//  Copyright © 2016 quadecco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
